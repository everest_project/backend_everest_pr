package api

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	v1 "gitlab.com/everest_project/backend/api/v1"
	"gitlab.com/everest_project/backend/config"
	logging "gitlab.com/everest_project/backend/pkg/logger"
	"gitlab.com/everest_project/backend/pkg/token"
	"gitlab.com/everest_project/backend/storage"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware

	_ "gitlab.com/everest_project/backend/api/docs" // for swagwger
)

type RouterOptions struct {
	Cfg        *config.Config
	Storage    storage.StorageI
	InMemory   storage.InMemoryStorageI
	TokenMaker token.Maker
	Logger     *logging.Logger
}

// New @title           Swagger doc for test taking website api
// @version         1.0
// @description     This is a api Swagger Doc.
// @BasePath  /v1
// @securityDefinitions.apikey Bearer
// @in header
// @name Authorization
// @description Type "Bearer" follows by a space and JWT token typed then.
func New(opt *RouterOptions) *gin.Engine {
	router := gin.Default()

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowCredentials = true
	corsConfig.AllowHeaders = append(corsConfig.AllowHeaders, "*")
	router.Use(cors.New(corsConfig))

	handlerV1 := v1.New(&v1.HandlerV1Options{
		Cfg:        opt.Cfg,
		Storage:    opt.Storage,
		InMemory:   opt.InMemory,
		TokenMaker: opt.TokenMaker,
		Logger:     opt.Logger,
	})

	router.Static("/media", "./media")

	apiV1 := router.Group("/v1")

	apiV1.GET("/users/:id", handlerV1.AuthMiddleware("users", "get"), handlerV1.GetUser)
	apiV1.POST("/users", handlerV1.AuthMiddleware("users", "create"), handlerV1.CreateUser)
	apiV1.GET("/users", handlerV1.AuthMiddleware("users", "get-all"), handlerV1.GetAllUsers)
	apiV1.PUT("/users/:id", handlerV1.AuthMiddleware("users", "update"), handlerV1.UpdateUser)
	apiV1.DELETE("/users/:id", handlerV1.AuthMiddleware("users", "delete"), handlerV1.DeleteUser)
	apiV1.GET("/users/me", handlerV1.AuthMiddleware("users", "get-profile"), handlerV1.GetUserProfile)

	apiV1.POST("/categories", handlerV1.AuthMiddleware("categories", "create"), handlerV1.CreateCategory)
	apiV1.GET("/categories", handlerV1.GetAllCategories)
	apiV1.PUT("/categories/:id", handlerV1.AuthMiddleware("categories", "update"), handlerV1.UpdateCategory)
	apiV1.DELETE("/categories/:id", handlerV1.AuthMiddleware("categories", "delete"), handlerV1.DeleteCategory)

	apiV1.POST("/tests", handlerV1.AuthMiddleware("tests", "add"), handlerV1.AddTests)
	apiV1.GET("/tests", handlerV1.GetRandomTests)
	apiV1.GET("/tests/get-one/:id", handlerV1.GetSingleTest)
	apiV1.GET("/tests/by/category", handlerV1.AuthMiddleware("tests", "get-by-category"), handlerV1.GetTestsByCategory)
	apiV1.DELETE("/tests/many", handlerV1.AuthMiddleware("tests", "delete"), handlerV1.DeleteTests)
	apiV1.DELETE("/tests/one", handlerV1.AuthMiddleware("test", "delete"), handlerV1.DeleteTest)
	apiV1.PUT("/tests/:id", handlerV1.AuthMiddleware("tests", "update"), handlerV1.UpdateTest)
	apiV1.POST("/tests/image-upload", handlerV1.AuthMiddleware("tests", "image-upload"), handlerV1.UploadQuestionImage)
	apiV1.POST("/tests/check-answers", handlerV1.CheckTestsAnswers)

	apiV1.POST("/auth/register", handlerV1.Register)
	apiV1.POST("/auth/verify", handlerV1.Verify)
	apiV1.POST("/auth/login", handlerV1.Login)
	apiV1.DELETE("/auth/logout", handlerV1.AuthMiddleware("auth", "logout"), handlerV1.Logout)
	apiV1.DELETE("/auth/delete-account", handlerV1.AuthMiddleware("auth", "delete-account"), handlerV1.DeleteAccount)
	apiV1.POST("/auth/forgot-password", handlerV1.ForgotPassword)
	apiV1.POST("/auth/verify-forgot-password", handlerV1.VerifyForgotPassword)
	apiV1.POST("/auth/update-password", handlerV1.AuthMiddleware("auth", "update-password"), handlerV1.UpdatePassword)
	apiV1.POST("/auth/update-email", handlerV1.AuthMiddleware("auth", "update-email"), handlerV1.UpdateEmail)
	apiV1.POST("/auth/verify-update-email", handlerV1.VerifyUpdateEmail)
	apiV1.POST("/token/refresh", handlerV1.RenewAccessToken)

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return router
}
