package models

import "time"

type RegisterRequest struct {
	FirstName   string `json:"first_name" binding:"required,min=2,max=30"`
	LastName    string `json:"last_name" binding:"required,min=2,max=30"`
	PhoneNumber string `json:"phone_number"`
	Email       string `json:"email" binding:"required,email"`
	Username    string `json:"username"`
	Password    string `json:"password" binding:"required,min=6,max=16"`
}

type AuthResponse struct {
	ID          int64     `json:"id"`
	FirstName   string    `json:"first_name"`
	LastName    string    `json:"last_name"`
	Email       string    `json:"email"`
	Username    string    `json:"username"`
	Type        string    `json:"type"`
	CreatedAt   time.Time `json:"created_at"`
	AccessToken string    `json:"access_token"`
}

type AuthPayload struct {
	ID            string    `json:"id"`
	UserID        int64     `json:"user_id"`
	Email         string    `json:"email"`
	UserType      string    `json:"user_type"`
	IssuedAt      time.Time `json:"issued_at"`
	ExpiredAt     time.Time `json:"expired_at"`
	HasPermission bool      `json:"has_permission"`
}

type VerifyRequest struct {
	Email string `json:"email" binding:"required,email"`
	Code  string `json:"code" binding:"required"`
}

type LoginRequest struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required,min=6,max=16"`
}

type ForgotPasswordRequest struct {
	Email string `json:"email" binding:"required,email"`
}

type UpdatePasswordRequest struct {
	Password string `json:"password" binding:"required"`
}

type LoginRes struct {
	User          User     `json:"user"`
	TokenResponse TokenRes `json:"token_response"`
}

type UpdateEmailReq struct {
	Email string `json:"email" binding:"required"`
}