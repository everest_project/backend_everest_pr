package models

import (
	"mime/multipart"
	"time"

	"gitlab.com/everest_project/backend/storage/repo"
)

type TestsParams struct {
	CategoryID int64  `json:"category_id" binding:"required"`
	Difficulty string `json:"difficulty" binding:"required, oneof=medium hard"`
	Lang       string `json:"lang" binding:"required, oneof=uz ru"`
}

type GetTestsParams struct {
	CategoryID int64  `json:"category_id" binding:"required"`
	Difficulty string `json:"difficulty" binding:"oneof=medium hard"`
	Lang       string `json:"lang" binding:"oneof=uz ru"`
}

type Test struct {
	ID               int64          `json:"id"`
	AdminID          int64          `json:"admin_id"`
	CategoryID       int64          `json:"category_id"`
	Difficulty       string         `json:"difficulty"`
	Question         string         `json:"question"`
	QuestionImageUrl *string        `json:"question_image_url"`
	Options          []*repo.Option `json:"options"`
	AnswerOption     string         `json:"answer_option"`
	Points           int32          `json:"points"`
	Lang             string         `json:"lang"`
	CreatedAt        time.Time      `json:"created_at"`
}

type AddTestsReq struct {
	File       *multipart.FileHeader `form:"file"`
	CategoryID int64                 `form:"category_id" binding:"required"`
	Difficulty string                `form:"difficulty" binding:"required"`
	Lang       string                `form:"lang" binding:"required"`
}

type GetTestsResp struct {
	Tests []*Test `json:"tests"`
	Count int32   `json:"count"`
}

type DeleteTestReq struct {
	ID         int64 `json:"id" binding:"required"`
	CategoryID int64 `json:"category_id" binding:"required"`
}

type UpdateTestReq struct {
	CategoryID       int64          `json:"category_id"`
	Difficulty       string         `json:"difficulty"`
	Question         string         `json:"question"`
	QuestionImageUrl *string        `json:"question_image_url"`
	Options          []*repo.Option `json:"options"`
	AnswerOption     string         `json:"answer_option"`
	Points           int32          `json:"points"`
	Lang             string         `json:"lang"`
}
