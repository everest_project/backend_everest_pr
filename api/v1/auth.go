package v1

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"gitlab.com/everest_project/backend/api/models"
	emailPkg "gitlab.com/everest_project/backend/pkg/email"
	"gitlab.com/everest_project/backend/pkg/token"
	"gitlab.com/everest_project/backend/pkg/utils"
	"gitlab.com/everest_project/backend/storage/repo"
)

const (
	RegisterCodeKey   = "register_code_"
	ForgotPasswordKey = "forgot_password_code_"
)

// @Router /auth/register [post]
// @Summary Register a user
// @Description Register a user
// @Tags auth
// @Accept json
// @Produce json
// @Param data body models.RegisterRequest true "Data"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) Register(c *gin.Context) {
	var (
		req models.RegisterRequest
	)
	err := c.ShouldBindJSON(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json to user")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	if !validatePassword(req.Password) {
		h.logger.WithError(err).Error("failed to validate password")
		c.JSON(http.StatusBadRequest, errorResponse(ErrWeakPassword))
		return
	}

	res, _ := h.storage.User().GetByEmail(req.Email)
	if res != nil {
		h.logger.WithError(err).Error("failed to check user by email")
		c.JSON(http.StatusBadRequest, errorResponse(ErrEmailExists))
		return
	}

	hashedPassword, err := utils.HashPassword(req.Password)
	if err != nil {
		h.logger.WithError(err).Error("failed to hash password")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	user := repo.User{
		FirstName:   req.FirstName,
		LastName:    req.LastName,
		Username:    &req.Username,
		Email:       req.Email,
		PhoneNumber: &req.PhoneNumber,
		Password:    hashedPassword,
		Type:        repo.UserTypeUser,
	}

	userData, err := json.Marshal(user)
	if err != nil {
		h.logger.WithError(err).Error("failed to marshal json")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	err = h.inMemory.Set("user_"+user.Email, string(userData), 10*time.Minute)
	if err != nil {
		h.logger.WithError(err).Error("failed to set user data to redis")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	go func() {
		err := h.sendVerificationCode(RegisterCodeKey, req.Email)
		if err != nil {
			h.logger.WithError(err).Error("failed to send verfication code")
			fmt.Printf("Failed to send verification code: %v", err)
		}
	}()

	c.JSON(http.StatusCreated, models.ResponseOK{
		Message: "Verification code has been sent!",
	})

}

func validatePassword(password string) bool {
	var smallLetter, number bool

	for i := 0; i < len(password); i++ {
		if password[i] >= 97 && password[i] <= 122 {
			smallLetter = true
		} else if password[i] >= 48 && password[i] <= 57 {
			number = true
		}
	}

	return smallLetter && number
}

func (h *handlerV1) sendVerificationCode(key, email string) error {
	code, err := utils.GenerateRandomCode(6)
	if err != nil {
		return err
	}

	err = h.inMemory.Set(key+email, code, time.Minute*2)
	if err != nil {
		return err
	}

	err = emailPkg.SendEmail(h.cfg, &emailPkg.SendEmailRequest{
		To:      []string{email},
		Subject: "Verification email",
		Body: map[string]string{
			"code": code,
		},
		Type: emailPkg.VerificationEmail,
	})
	if err != nil {
		return err
	}

	return nil
}

// @Router /auth/verify [post]
// @Summary Verify email
// @Description Verify email
// @Tags auth
// @Accept json
// @Produce json
// @Param data body models.VerifyRequest true "Data"
// @Success 200 {object} models.AuthResponse
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) Verify(c *gin.Context) {
	var (
		req models.VerifyRequest
	)
	err := c.ShouldBindJSON(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed bind json to user")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	userData, err := h.inMemory.Get("user_" + req.Email)
	if err != nil {
		h.logger.WithError(err).Error("failed to get user data from redis")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	var user repo.User
	err = json.Unmarshal([]byte(userData), &user)
	if err != nil {
		h.logger.WithError(err).Error("failed to unmarshal user")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
	}

	code, err := h.inMemory.Get(RegisterCodeKey + user.Email)
	if err != nil {
		h.logger.WithError(err).Error("failed to get verfication code from redis")
		c.JSON(http.StatusBadRequest, errorResponse(ErrCodeExpired))
		return
	}

	if req.Code != code {
		c.JSON(http.StatusBadRequest, errorResponse(ErrWrongCode))
		return
	}

	result, err := h.storage.User().Create(&user)
	if err != nil {
		h.logger.WithError(err).Error("failed to create user")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	token, _, err := h.tokenMaker.CreateToken(&token.TokenParams{
		UserID:   result.ID,
		UserType: result.Type,
		Email:    result.Email,
		Duration: h.cfg.AccessTokenDuration,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to create token")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	c.JSON(http.StatusCreated, models.AuthResponse{
		ID:          user.ID,
		FirstName:   user.FirstName,
		LastName:    user.LastName,
		Email:       user.Email,
		Type:        user.Type,
		CreatedAt:   user.CreatedAt,
		AccessToken: token,
	})
}

// @Router /auth/login [post]
// @Summary Login user
// @Description Login user
// @Tags auth
// @Accept json
// @Produce json
// @Param data body models.LoginRequest true "Data"
// @Success 200 {object} models.AuthResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) Login(c *gin.Context) {
	var (
		req models.LoginRequest
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	user, err := h.storage.User().GetByEmail(req.Email)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			h.logger.WithError(err).Error("failed to get user by email")
			c.JSON(http.StatusForbidden, errorResponse(ErrWrongEmailOrPass))
			return
		}
		h.logger.WithError(err).Error("failed get user by email")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	err = utils.CheckPassword(req.Password, user.Password)
	if err != nil {
		h.logger.WithError(err).Error("failed on checking password")
		c.JSON(http.StatusForbidden, errorResponse(ErrWrongEmailOrPass))
		return
	}
	acessToken, _, err := h.tokenMaker.CreateToken(&token.TokenParams{
		UserID:   user.ID,
		Email:    user.Email,
		UserType: user.Type,
		Duration: h.cfg.AccessTokenDuration,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed access create token")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	refreshToken, refreshPayload, err := h.tokenMaker.CreateToken(&token.TokenParams{
		UserID:   user.ID,
		Email:    user.Email,
		UserType: user.Type,
		Duration: h.cfg.RefreshTokenDuration,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed create refresh token")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	_, err = h.storage.Session().Create(&repo.Session{
		ID:           refreshPayload.ID,
		Email:        req.Email,
		RefreshToken: refreshToken,
		UserAgent:    c.Request.UserAgent(),
		ClientIP:     c.ClientIP(),
		IsBlocked:    false,
		ExpiresAt:    refreshPayload.ExpiresAt,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed create user")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, models.LoginRes{
		TokenResponse: models.TokenRes{
			AccessToken:  acessToken,
			RefreshToken: refreshToken,
		},
		User: models.User{
			ID:          user.ID,
			FirstName:   user.FirstName,
			LastName:    user.LastName,
			Email:       user.Email,
			PhoneNumber: user.PhoneNumber,
			Username:    user.Username,
			Type:        user.Type,
			CreatedAt:   user.CreatedAt,
		},
	})
}

// @Security Bearer
// @Router /auth/logout [delete]
// @Summary Logout from an account
// @Description Logout from an account
// @Tags auth
// @Accept json
// @Produce json
// @Success 200 {object} models.ResponseOK
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) Logout(c *gin.Context) {
	payload, err := h.GetAuthPayload(c)
	if err != nil {
		h.logger.WithError(err).Error("failed to get payload")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	session, err := h.storage.Session().GetByEmail(&repo.GetSessionParams{Email: payload.Email})
	if err != nil {
		h.logger.WithError(err).Error("failed to log out")
		c.JSON(http.StatusCreated, errorResponse(err))
		return

	}

	err = h.storage.Session().Delete(&repo.GetSessionParams{
		Email:     session.Email,
		UserAgent: c.Request.UserAgent(),
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to delete user's session")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, models.ResponseOK{
		Message: "logged out successfully",
	})
}

// @Security Bearer
// @Router /auth/delete-account [delete]
// @Summary Delete an account
// @Description Delete an account
// @Tags auth
// @Accept json
// @Produce json
// @Success 200 {object} models.AuthResponse
// @Failure 404 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) DeleteAccount(c *gin.Context) {
	payload, err := h.GetAuthPayload(c)
	if err != nil {
		h.logger.WithError(err).Error("failed to get payload")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	user, err := h.storage.User().GetByEmail(payload.Email)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			h.logger.WithError(err).Error("failed to get user by email")
			c.JSON(http.StatusNotFound, errorResponse(err))
			return
		} else {
			h.logger.WithError(err).Error("failed to get user by email")
			c.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
	}
	err = h.storage.User().Delete(user.ID)
	if err != nil {
		h.logger.WithError(err).Error("failed to delete user")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	session, err := h.storage.Session().GetByEmail(&repo.GetSessionParams{Email: payload.Email})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			h.logger.WithError(err).Error("failed to log out")
			c.JSON(http.StatusOK, models.ResponseOK{
				Message: "Your account has been successfully deleted!",
			})
			return
		} else {
			h.logger.WithError(err).Error("failed check session")
			c.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
	}

	err = h.storage.Session().Delete(&repo.GetSessionParams{
		Email:     session.Email,
		UserAgent: c.Request.UserAgent(),
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to delete user's session")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
}

// @Router /auth/forgot-password [post]
// @Summary Forgot password
// @Description Forgot password
// @Tags auth
// @Accept json
// @Produce json
// @Param data body models.ForgotPasswordRequest true "Data"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) ForgotPassword(c *gin.Context) {
	var (
		req models.ForgotPasswordRequest
	)
	err := c.ShouldBindJSON(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	_, err = h.storage.User().GetByEmail(req.Email)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			h.logger.WithError(err).Error("failed to get user by email")
			c.JSON(http.StatusNotFound, errorResponse(err))
			return
		}
		h.logger.WithError(err).Error("failed to get user by email")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	go func() {
		err := h.sendVerificationCode(ForgotPasswordKey, req.Email)
		if err != nil {
			h.logger.WithError(err).Error("failed to send verification code")
			fmt.Printf("Failed to send verification code: %v", err)
		}
	}()

	c.JSON(http.StatusCreated, models.ResponseOK{
		Message: "Verification code has been sent!",
	})

}

// @Router /auth/verify-forgot-password [post]
// @Summary Verify forgot password
// @Description Verify forgot password
// @Tags auth
// @Accept json
// @Produce json
// @Param data body models.VerifyRequest true "Data"
// @Success 200 {object} models.AuthResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) VerifyForgotPassword(c *gin.Context) {
	var (
		req models.VerifyRequest
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed bind json")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	code, err := h.inMemory.Get(ForgotPasswordKey + req.Email)
	if err != nil {
		h.logger.WithError(err).Error("failed to get user data from redis")
		c.JSON(http.StatusForbidden, errorResponse(ErrCodeExpired))
		return
	}

	if req.Code != code {
		h.logger.WithError(err).Error("failed when checking verification codes")
		c.JSON(http.StatusForbidden, errorResponse(ErrIncorrectCode))
		return
	}

	result, err := h.storage.User().GetByEmail(req.Email)
	if err != nil {
		h.logger.WithError(err).Error("failed to get user by email")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	token, _, err := h.tokenMaker.CreateToken(&token.TokenParams{
		UserID:   result.ID,
		Email:    result.Email,
		UserType: result.Type,
		Duration: h.cfg.AccessTokenDuration,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed access create token")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, models.AuthResponse{
		ID:          result.ID,
		FirstName:   result.FirstName,
		LastName:    result.LastName,
		Email:       result.Email,
		Type:        result.Type,
		CreatedAt:   result.CreatedAt,
		AccessToken: token,
	})

}

// @Security Bearer
// @Router /auth/update-password [post]
// @Summary Update password
// @Description Update password
// @Tags auth
// @Accept json
// @Produce json
// @Param data body models.UpdatePasswordRequest true "Data"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UpdatePassword(c *gin.Context) {
	var (
		req models.UpdatePasswordRequest
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	payload, err := h.GetAuthPayload(c)
	if err != nil {
		h.logger.WithError(err).Error("failed get auth payload")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	user, err := h.storage.User().Get(payload.UserID)
	if err != nil {
		h.logger.WithError(err).Error("failed to get user")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	if err := utils.CheckPassword(req.Password, user.Password); err == nil {
		h.logger.WithError(err).Error("passwords are identical")
		c.JSON(http.StatusInternalServerError, errorResponse(ErrIdenticalPasswords))
		return
	}

	hashedPassword, err := utils.HashPassword(req.Password)
	if err != nil {
		h.logger.WithError(err).Error("failed to hash password")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	err = h.storage.User().UpdatePassword(&repo.UpdatePassword{
		UserID:   payload.UserID,
		Password: hashedPassword,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to update user password")
		c.JSON(http.StatusInternalServerError, errorResponse)
	}

	c.JSON(http.StatusCreated, models.ResponseOK{
		Message: "Password has been updated!",
	})
}

// @Security Bearer
// @Router /auth/update-email [post]
// @Summary Update email
// @Description Update email
// @Tags auth
// @Accept json
// @Produce json
// @Param data body models.UpdateEmailReq true "Data"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UpdateEmail(c *gin.Context) {
	var (
		req models.UpdateEmailReq
	)

	if err := c.ShouldBindJSON(&req); err != nil {
		h.logger.WithError(err).Error("failed to bind json")
	}

	payload, err := h.GetAuthPayload(c)
	if err != nil {
		h.logger.WithError(err).Error("failed get auth payload")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	if payload.Email == req.Email {
		h.logger.WithError(err).Error("it isn't possible to change email to current email")
		c.JSON(http.StatusMethodNotAllowed, models.ResponseOK{
			Message: "it isn't possible to change email to present email",
		})
		return
	}
	oldEmail := payload.Email
	payload.Email = req.Email

	payloadData, err := json.Marshal(payload)
	if err != nil {
		h.logger.WithError(err).Error("failed to marshal json")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	email, err := json.Marshal(oldEmail)
	if err != nil {
		h.logger.WithError(err).Error("failed to marshal json")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	err = h.inMemory.Set("user_"+req.Email, string(payloadData), 10*time.Minute)
	if err != nil {
		h.logger.WithError(err).Error("failed to set user data to redis")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	err = h.inMemory.Set("old_email_", string(email), 10*time.Minute)
	if err != nil {
		h.logger.WithError(err).Error("failed to set user data to redis")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	go func() {
		err := h.sendVerificationCode(RegisterCodeKey, req.Email)
		if err != nil {
			h.logger.WithError(err).Error("failed to send verfication code")
			fmt.Printf("Failed to send verification code: %v", err)
		}
	}()

	c.JSON(http.StatusCreated, models.ResponseOK{
		Message: "Verification code has been sent!",
	})
}

// @Security Bearer
// @Router /auth/verify-update-email [post]
// @Summary Update email
// @Description Update email
// @Tags auth
// @Accept json
// @Produce json
// @Param data body models.VerifyRequest true "Data"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) VerifyUpdateEmail(c *gin.Context) {
	var (
		req models.VerifyRequest
	)
	err := c.ShouldBindJSON(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed bind json to user")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	payloadData, err := h.inMemory.Get("user_" + req.Email)
	if err != nil {
		h.logger.WithError(err).Error("failed to get user data from redis")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	var payload token.Payload
	err = json.Unmarshal([]byte(payloadData), &payload)
	if err != nil {
		h.logger.WithError(err).Error("failed to unmarshal user")
		c.JSON(http.StatusForbidden, errorResponse(err))
	}

	code, err := h.inMemory.Get(RegisterCodeKey + req.Email)
	if err != nil {
		h.logger.WithError(err).Error("failed to get verfication code from redis")
		c.JSON(http.StatusForbidden, errorResponse(ErrCodeExpired))
		return
	}

	if req.Code != code {
		h.logger.WithError(err).Error("failed while checking verification code")
		c.JSON(http.StatusUnauthorized, errorResponse(err))
		return
	}

	err = h.storage.User().UpdateEmail(&repo.UpdateEmail{
		Email: payload.Email,
		ID:    payload.UserID,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to update user's email")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	oldEmail, err := h.inMemory.Get("old_email_")
	if err != nil {
		h.logger.WithError(err).Error("failed to get old email from redis")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	var email string
	err = json.Unmarshal([]byte(oldEmail), &email)
	if err != nil {
		h.logger.WithError(err).Error("failed to unmarshal user")
		c.JSON(http.StatusForbidden, errorResponse(err))
	}
	session, err := h.storage.Session().GetByEmail(&repo.GetSessionParams{Email: email})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			c.JSON(http.StatusCreated, models.ResponseOK{
				Message: "login",
			})
			return
		} else if session == nil {
			c.JSON(http.StatusCreated, models.ResponseOK{
				Message: "login",
			})
			return
		}
	}
	err = h.storage.Session().Delete(&repo.GetSessionParams{Email: session.Email})
	if err != nil {
		h.logger.WithError(err).Error("failed to delete user's session")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	c.JSON(http.StatusCreated, models.ResponseOK{
		Message: "login",
	})
}
