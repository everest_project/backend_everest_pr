package v1

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/everest_project/backend/api/models"
	"gitlab.com/everest_project/backend/storage/repo"
)

// @Security Bearer
// @Router /categories [post]
// @Summary Create category
// @Description Craete category
// @Tags category
// @Accept json
// @Produce json
// @Param data body models.CategoryReq true "Data"
// @Success 201 {object} models.Category
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreateCategory(c *gin.Context) {
	var (
		req models.CategoryReq
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	resp, err := h.storage.Category().Create(&repo.Category{
		Title: req.Title,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to create user")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, parseCategoryModel(resp))
}

func parseCategoryModel(c *repo.Category) models.Category {
	return models.Category{
		ID:        c.ID,
		Title:     c.Title,
		CreatedAt: c.CreatedAt,
	}
}

// @Router /categories [get]
// @Summary Get all categories
// @Description Get all categories
// @Tags category
// @Accept json
// @Produce json
// @Param title query string false "Title"
// @Success 200 {object} models.GetAllCategoriesResp
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllCategories(c *gin.Context) {
	title := c.Query("title")

	resp, err := h.storage.Category().GetAll(&repo.GetAllCategoriesParam{
		Title: title,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to get all categories")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, getCategoriesResp(resp))
}

func getCategoriesResp(data *repo.GetAllCategoriesRes) *models.GetAllCategoriesResp {
	response := &models.GetAllCategoriesResp{
		Categories: make([]*models.Category, 0),
		Count:      data.Count,
	}

	for _, category := range data.Categories {
		c := parseCategoryModel(category)
		response.Categories = append(response.Categories, &c)
	}

	return response
}

// @Security Bearer
// @Router /categories/{id} [put]
// @Summary Update category
// @Description Update category
// @Tags category
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param category body models.CategoryReq true "Category"
// @Success 200 {object} models.Category
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UpdateCategory(c *gin.Context) {
	var (
		req models.CategoryReq
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		h.logger.WithError(err).Error("failed to get id")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	resp, err := h.storage.Category().Update(&repo.Category{
		Title: req.Title,
		ID:    id,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to update category")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, parseCategoryModel(resp))
}

// @Security Bearer
// @Router /categories/{id} [delete]
// @Summary Delete a category
// @Description Delete a category
// @Tags category
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) DeleteCategory(c *gin.Context) {
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		h.logger.WithError(err).Error("failed to parse id")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	
	data, err := h.storage.Test().GetTestsByCategory(&repo.GetTestsParams{
		CategoryID: id,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to get tests by category ID")
		c.JSON(http.StatusNotFound, errorResponse(err))
		return
	}

	for i := 0; i < len(data.Tests); i++ {
		err := h.storage.Test().DeleteImages(data.Tests[i])
		if err != nil {
			h.logger.WithError(err).Error("failed to delete tests' images")
			c.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
	}

	err = h.storage.Category().Delete(id)
	if err != nil {
		h.logger.WithError(err).Error("failed to delete category")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, models.ResponseOK{
		Message: "Category has been successfully deleted!",
	})
}
