package v1

import (
	"errors"
	"strconv"

	"gitlab.com/everest_project/backend/api/models"
	"gitlab.com/everest_project/backend/config"
	logging "gitlab.com/everest_project/backend/pkg/logger"
	"gitlab.com/everest_project/backend/pkg/token"
	"gitlab.com/everest_project/backend/storage"

	"github.com/gin-gonic/gin"
)

var (
	ErrWrongEmailOrPass   = errors.New("wrong email or password")
	ErrIncorrectCode      = errors.New("incorrect verification code")
	ErrCodeExpired        = errors.New("verification code has been expired")
	ErrEmailExists        = errors.New("email already exists")
	ErrForbidden          = errors.New("forbidden")
	ErrUserNotVerified    = errors.New("user not verified")
	ErrNotAllowed         = errors.New("method not allowed")
	ErrWeakPassword       = errors.New("password must contain at least one small letter, one capital letter, one number, one symbol")
	ErrIdenticalPasswords = errors.New("passwords are identical")
	ErrWrongCode          = errors.New("incorrect verification code")
)

type handlerV1 struct {
	cfg        *config.Config
	storage    storage.StorageI
	inMemory   storage.InMemoryStorageI
	tokenMaker token.Maker
	logger     *logging.Logger
}

type HandlerV1Options struct {
	Cfg        *config.Config
	Storage    storage.StorageI
	InMemory   storage.InMemoryStorageI
	TokenMaker token.Maker
	Logger     *logging.Logger
}

func New(options *HandlerV1Options) *handlerV1 {
	return &handlerV1{
		cfg:        options.Cfg,
		storage:    options.Storage,
		inMemory:   options.InMemory,
		tokenMaker: options.TokenMaker,
		logger:     options.Logger,
	}
}

func errorResponse(err error) *models.ErrorResponse {
	return &models.ErrorResponse{
		Error: err.Error(),
	}
}

func validateGetAllUsersParams(c *gin.Context) (*models.GetAllParams, error) {
	var (
		limit int = 10
		page  int = 1
		err   error
	)

	if c.Query("limit") != "" {
		limit, err = strconv.Atoi(c.Query("limit"))
		if err != nil {
			return nil, err
		}
	}

	if c.Query("page") != "" {
		page, err = strconv.Atoi(c.Query("page"))
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllParams{
		Limit:  int32(limit),
		Page:   int32(page),
		Search: c.Query("search"),
	}, nil
}

func validateTestsParams(c *gin.Context) (*models.TestsParams, error) {
	var (
		categoryID int
		difficulty string
		lang       string
		err        error
	)
	if c.Query("category_id") != "" {
		categoryID, err = strconv.Atoi(c.Query("category_id"))
		if err != nil {
			return nil, err
		}
	}
	if c.Query("difficulty") != "" {
		difficulty = c.Query("difficulty")
	}
	if c.Query("lang") != "" {
		lang = c.Query("lang")
	}

	return &models.TestsParams{
		CategoryID: int64(categoryID),
		Difficulty: difficulty,
		Lang:       lang,
	}, nil
}

func validateDeleteTestParams(c *gin.Context) (*models.DeleteTestReq, error) {
	var (
		id         int
		categoryID int
		err        error
	)
	if c.Query("id") != "" {
		id, err = strconv.Atoi(c.Query("id"))
		if err != nil {
			return nil, err
		}
	}

	if c.Query("category_id") != "" {
		categoryID, err = strconv.Atoi(c.Query("category_id"))
		if err != nil {
			return nil, err
		}
	}

	return &models.DeleteTestReq{
		ID:         int64(id),
		CategoryID: int64(categoryID),
	}, nil
}
