package v1

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/everest_project/backend/api/models"
)

const (
	authorizationHeaderKey  = "authorization"
	authorizationTypeBearer = "bearer"
	authorizationPayloadKey = "authorization_payload"
)

type Payload struct {
	Id        string `json:"id"`
	UserID    int64  `json:"user_id"`
	Email     string `json:"email"`
	UserType  string `json:"user_type"`
	IssuedAt  string `json:"issued_at"`
	ExpiredAt string `json:"expired_at"`
}

func (h *handlerV1) AuthMiddleware(resource, action string) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		authorizationHeader := ctx.GetHeader(authorizationHeaderKey)

		if len(authorizationHeader) == 0 {
			err := errors.New("authorization header is not provided")
			h.logger.WithError(err).Error("authorization header is not provided")
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, errorResponse(err))
			return
		}

		fields := strings.Fields(authorizationHeader)
		if len(fields) < 2 {
			err := errors.New("invalid authorization header format")
			h.logger.WithError(err).Error("invalid authorization header format")
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, errorResponse(err))
		}

		authorizationType := strings.ToLower(fields[0])
		if authorizationType != authorizationTypeBearer {
			err := fmt.Errorf("unsupported authorization type %s", authorizationType)
			h.logger.WithError(err).Error("unsupported authorization type")
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, errorResponse(err))
			return
		}
		accessToken := fields[1]
		payload, err := h.VerifyToken(accessToken, resource, action)
		if err != nil {
			h.logger.WithError(err).Error("failed to verify token")
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, errorResponse(err))
			return
		}
		if !payload.HasPermission {
			ctx.AbortWithStatusJSON(http.StatusForbidden, errorResponse(ErrNotAllowed))
			return
		}
		
		user, err := h.storage.User().GetByEmail(payload.Email)
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				h.logger.WithError(err).Error("failed to get user bc user doesn't exists")
				ctx.AbortWithStatusJSON(http.StatusBadRequest, errorResponse(err))
				return
			} else if user == nil {
				h.logger.WithError(err).Error("failed to get user bc user doesn't exists")
				ctx.AbortWithStatusJSON(http.StatusInternalServerError, errorResponse(err))
				return
			} else {
				h.logger.WithError(err).Error("failed to get user by email in the middleware")
				ctx.AbortWithStatusJSON(http.StatusInternalServerError, errorResponse(err))
				return
			}
		}

		ctx.Set(authorizationPayloadKey, payload)
		ctx.Next()
	}
}

func (h *handlerV1) GetAuthPayload(ctx *gin.Context) (*Payload, error) {
	i, exists := ctx.Get(authorizationPayloadKey)
	if !exists {
		h.logger.Error("not found")
		return nil, errors.New("not found")
	}

	payload, ok := i.(*models.AuthPayload)
	if !ok {
		h.logger.Error("unknown user")
		return nil, errors.New("unknown user")
	}
	return &Payload{
		Id:        payload.ID,
		UserID:    payload.UserID,
		Email:     payload.Email,
		UserType:  payload.UserType,
		IssuedAt:  payload.IssuedAt.Format(time.RFC3339),
		ExpiredAt: payload.ExpiredAt.Format(time.RFC3339),
	}, nil
}

func (h *handlerV1) VerifyToken(accessToken, resource, action string) (*models.AuthPayload, error) {
	payload, err := h.tokenMaker.VerifyToken(accessToken)
	if err != nil {
		h.logger.WithError(err).Error("failed to verify token")
		return nil, err
	}

	hasPermission, err := h.storage.Permission().CheckPermission(payload.UserType, resource, action)
	if err != nil {
		h.logger.WithError(err).Error("failed to check permission")
		return nil, err
	}

	return &models.AuthPayload{
		ID:            payload.ID.String(),
		UserID:        payload.UserID,
		Email:         payload.Email,
		UserType:      payload.UserType,
		IssuedAt:      payload.IssuedAt,
		ExpiredAt:     payload.ExpiresAt,
		HasPermission: hasPermission,
	}, nil
}
