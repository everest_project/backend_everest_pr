package v1

import (
	"database/sql"
	"errors"
	"fmt"
	"math/rand"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/xuri/excelize/v2"
	"gitlab.com/everest_project/backend/api/models"
	"gitlab.com/everest_project/backend/storage/repo"
)

type File struct {
	File *multipart.FileHeader `form:"file" binding:"required"`
}

const (
	xlsxFormat = ".xlsx"
)

// @Security Bearer
// @Router /tests [post]
// @Summary Add tests to a specific category choosing difficulty and language
// @Description Upload the tests in .xlsx file to add them
// @Tags test
// @Accept json
// @Produce json
// @Param category_id formData int true "Category ID"
// @Param difficulty formData string true "Difficulty"
// @Param lang formData string true "Language"
// @Param file formData file true "File"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) AddTests(c *gin.Context) {
	var (
		req models.AddTestsReq
	)
	err := c.ShouldBind(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind formdata")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	payload, err := h.GetAuthPayload(c)
	if err != nil {
		h.logger.WithError(err).Error("failed to get authorization payload")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	excelFilesDst, err := prepareDestionationDir("/sheets/")
	if err != nil {
		h.logger.WithError(err).Error("failed to prepare destination directory")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	defer os.RemoveAll(excelFilesDst)

	fileName, err := h.saveXlsxFile(c, excelFilesDst, req.File)
	if err != nil {
		h.logger.WithError(err).Error("failed to save uploaded file")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	defer os.Remove(excelFilesDst + fileName)

	f, err := excelize.OpenFile(excelFilesDst + fileName)
	if err != nil {
		h.logger.WithError(err).Error("failed to open the file")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	defer f.Close()
	sheets := f.GetSheetList()
	rows, err := f.GetRows(sheets[0])
	if err != nil {
		h.logger.WithError(err).Error("failed to get rows")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	res := make([]repo.Test, 0, len(rows)-1)
	var test repo.Test

	for index, row := range rows[1:] {
		if len(row) != 0 {
			imgUrls, err := h.processOptions(c, f, sheets[0], row[2], row[3], row[4], row[5], index)
			if err != nil {
				h.logger.WithError(err).Error("failed while processing the option pics")
				c.JSON(http.StatusInternalServerError, errorResponse(err))
				return
			}
			options := []*repo.Option{
				{ID: 1, Title: row[2], ImageUrl: &imgUrls[0]},
				{ID: 2, Title: row[3], ImageUrl: &imgUrls[1]},
				{ID: 3, Title: row[4], ImageUrl: &imgUrls[2]},
				{ID: 4, Title: row[5], ImageUrl: &imgUrls[3]},
			}
			points, err := strconv.Atoi(row[7])
			if err != nil {
				h.logger.WithError(err).Error("failed to parse to int")
				c.JSON(http.StatusInternalServerError, errorResponse(err))
				return
			}
			questionPicCell := fmt.Sprintf("B%d", 2+index)
			iname, raw, _ := f.GetPicture(sheets[0], questionPicCell)
			if iname != "" && raw != nil {
				slice := strings.Split(iname, ".")
				name := uuid.New().String() + "." + slice[1]

				dst, err := prepareDestionationDir("/media/")
				if err != nil {
					h.logger.WithError(err).Error("failed to prepare dst")
					c.JSON(http.StatusInternalServerError, errorResponse(err))
					return
				}
				imageUrl, err := h.savePic(dst, name, raw)
				if err != nil {
					h.logger.WithError(err).Error("failed to save pic")
					c.JSON(http.StatusInternalServerError, errorResponse(err))
					return
				}

				test = repo.Test{
					AdminID:          payload.UserID,
					CategoryID:       req.CategoryID,
					Difficulty:       req.Difficulty,
					Question:         row[0],
					QuestionImageUrl: &imageUrl,
					Options:          options,
					AnswerOption:     row[6],
					Points:           int32(points),
					Lang:             req.Lang,
				}
				res = append(res, test)
			} else {
				test = repo.Test{
					AdminID:      payload.UserID,
					CategoryID:   req.CategoryID,
					Difficulty:   req.Difficulty,
					Question:     row[0],
					Options:      options,
					AnswerOption: row[6],
					Points:       int32(points),
					Lang:         req.Lang,
				}

				res = append(res, test)
			}

		} else {
			continue
		}
	}

	err = h.storage.Test().MultipleInsert(res)
	if err != nil {
		h.logger.WithError(err).Error("failed to create tests")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, models.ResponseOK{
		Message: "Tests has been successfully added!",
	})
}

func prepareDestionationDir(dir string) (string, error) {
	dst, _ := os.Getwd()
	if _, err := os.Stat(dst + dir); os.IsNotExist(err) {
		if err := os.Mkdir(dst+dir, os.ModePerm); err != nil {
			return "", err
		}
	}
	return dst + dir, nil
}

func (h *handlerV1) saveXlsxFile(c *gin.Context, dst string, file *multipart.FileHeader) (string, error) {
	id := uuid.New()
	fileName := id.String() + filepath.Ext(file.Filename)

	if filepath.Ext(file.Filename) != xlsxFormat {
		h.logger.Error("incompatible file format")
		return "", errors.New("incompatible file format")
	} else if err := c.SaveUploadedFile(file, dst+fileName); err != nil {
		h.logger.WithError(err).Error("failed to save file")
		return "", err
	}

	return fileName, nil
}

func (h *handlerV1) savePic(dst, name string, raw []byte) (string, error) {
	err := os.WriteFile(dst+name, raw, 0644)
	if err != nil {
		h.logger.WithError(err).Error("failed to write a file")
		return "", err
	}

	imageUrl := "/media/" + name

	return imageUrl, nil
}

func (h *handlerV1) processOptions(ctx *gin.Context, f *excelize.File, sheet, a, b, c, d string, loopIndex int) ([]string, error) {
	// Initialization of options' cell pathes
	optionAPic := fmt.Sprintf("C%d", 2+loopIndex)
	optionBPic := fmt.Sprintf("D%d", 2+loopIndex)
	optionCPic := fmt.Sprintf("E%d", 2+loopIndex)
	optionDPic := fmt.Sprintf("F%d", 2+loopIndex)
	paths := []string{optionAPic, optionBPic, optionCPic, optionDPic}

	for i := 0; i < 4; i++ {
		imgUrl, err := h.processOptionPic(f, sheet, paths[i])
		if err != nil {
			h.logger.WithError(err).Error("failed to process option's image")
			return nil, err
		}
		paths[i] = imgUrl
	}

	return paths, nil
}

func (h *handlerV1) processOptionPic(f *excelize.File, sheet, imagePath string) (string, error) {
	iname, raw, err := f.GetPicture(sheet, imagePath)
	if err != nil {
		h.logger.WithError(err).Error("failed to get picture from an excel file")
		return "", err
	} else if iname != "" && raw != nil {
		slice := strings.Split(iname, ".")
		name := uuid.New().String() + "." + slice[1]

		dst, _ := prepareDestionationDir("/media/")
		imgName, err := h.savePic(dst, name, raw)
		if err != nil {
			h.logger.WithError(err).Error("failed to save picture")
			return "", err
		}
		return imgName, nil
	}

	return "", nil
}

// @Router /tests [get]
// @Summary Get random tests
// @Description Get random tests by category and difficulty
// @Tags test
// @Accept json
// @Produce json
// @Param filter query models.TestsParams false "Filter"
// @Success 200 {object} models.GetTestsResp
// @Failure 404 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetRandomTests(c *gin.Context) {
	req, err := validateTestsParams(c)
	if err != nil {
		h.logger.WithError(err).Error("failed to get params")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	result, err := h.storage.Test().Get30Tests(&repo.GetTestsParams{
		CategoryID: req.CategoryID,
		Difficulty: req.Difficulty,
		Lang:       req.Lang,
	})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			h.logger.WithError(err).Error("tests not found")
			c.JSON(http.StatusNotFound, errorResponse(err))
			return
		} else {
			h.logger.WithError(err).Error("failed get random tests")
			c.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
	}

	for i := 0; i < len(result.Tests); i++ {
		result.Tests[i] = h.makeFullUrls(result.Tests[i])
	}

	c.JSON(http.StatusOK, getTestsResponse(result))
}

func (h *handlerV1) makeFullUrls(data *repo.Test) *repo.Test {
	sDomain := h.cfg.ServerDomain
	questionImageUrl := ""
	for j := 0; j < 4; j++ {
		optionImageUrl := ""
		if data.Options[j].ImageUrl != nil && *data.Options[j].ImageUrl != "" {
			optionImageUrl = sDomain + *data.Options[j].ImageUrl
			data.Options[j].ImageUrl = &optionImageUrl
			continue
		}
		data.Options[j].ImageUrl = &optionImageUrl
	}
	if data.QuestionImageUrl != nil {
		questionImageUrl = sDomain + *data.QuestionImageUrl
	}
	data.QuestionImageUrl = &questionImageUrl

	return data
}

func getTestsResponse(data *repo.GetTestsRes) *models.GetTestsResp {
	response := models.GetTestsResp{
		Tests: make([]*models.Test, 0),
		Count: data.Count,
	}

	for _, test := range data.Tests {
		t := parseTestModelWitRandom(test)
		response.Tests = append(response.Tests, t)
	}

	return &response
}

func parseTestModelWitRandom(data *repo.Test) *models.Test {
	for i := range data.Options {
		j := rand.Intn(i + 1)
		data.Options[i], data.Options[j] = data.Options[j], data.Options[i]
	}
	return &models.Test{
		ID:               data.ID,
		AdminID:          data.AdminID,
		CategoryID:       data.CategoryID,
		Difficulty:       data.Difficulty,
		Question:         data.Question,
		QuestionImageUrl: data.QuestionImageUrl,
		Options:          data.Options,
		AnswerOption:     data.AnswerOption,
		Points:           data.Points,
		Lang:             data.Lang,
		CreatedAt:        data.CreatedAt,
	}
}

func parseTestModel(data *repo.Test) *models.Test {
	return &models.Test{
		ID:               data.ID,
		AdminID:          data.AdminID,
		CategoryID:       data.CategoryID,
		Difficulty:       data.Difficulty,
		Question:         data.Question,
		QuestionImageUrl: data.QuestionImageUrl,
		Options:          data.Options,
		AnswerOption:     data.AnswerOption,
		Points:           data.Points,
		Lang:             data.Lang,
		CreatedAt:        data.CreatedAt,
	}
}

// @Security Bearer
// @Router /tests/many [delete]
// @Summary Delete tests by category_id, difficulty, lang
// @Description Delete multiple tests by category_id, difficulty, language
// @Tags test
// @Accept json
// @Produce json
// @Param params query models.TestsParams true "Params"
// @Success 200 {object} models.ResponseOK
// @Failure 404 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) DeleteTests(c *gin.Context) {
	req, err := validateTestsParams(c)
	if err != nil {
		h.logger.WithError(err).Error("failed get params")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	err = h.storage.Test().DeleteByCategoryNDifficulty(
		req.CategoryID,
		req.Difficulty,
		req.Lang,
	)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			h.logger.WithError(err).Error("tests not found")
			c.JSON(http.StatusNotFound, errorResponse(err))
			return
		} else {
			h.logger.WithError(err).Error("failed delete tests")
			c.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
	}

	c.JSON(http.StatusOK, models.ResponseOK{
		Message: "Tests has been successfully deleted!",
	})
}

// @Security Bearer
// @Router /tests/one [delete]
// @Summary Delete test
// @Description Delete a single test by id and category_id
// @Tags test
// @Accept json
// @Produce json
// @Param params query models.DeleteTestReq true "Params"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) DeleteTest(c *gin.Context) {
	req, err := validateDeleteTestParams(c)
	if err != nil {
		h.logger.WithError(err).Error("failed to validate params")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	err = h.storage.Test().Delete(req.ID, req.CategoryID)
	if err != nil {
		h.logger.WithError(err).Error("failed to delete test")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, models.ResponseOK{
		Message: "successfully deleted",
	})
}

// @Security Bearer
// @Router /tests/{id} [put]
// @Summary Update test
// @Description Update a single test by id and category_id
// @Tags test
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param test body models.UpdateTestReq true "Test"
// @Success 200 {object} models.Test
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UpdateTest(c *gin.Context) {
	var (
		req models.UpdateTestReq
	)

	err := c.ShouldBind(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		h.logger.WithError(err).Error("failed get an id")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	updated, err := h.storage.Test().Update(&repo.Test{
		ID:               int64(id),
		CategoryID:       req.CategoryID,
		Difficulty:       req.Difficulty,
		Question:         req.Question,
		QuestionImageUrl: req.QuestionImageUrl,
		Options:          req.Options,
		AnswerOption:     req.AnswerOption,
		Points:           req.Points,
		Lang:             req.Lang,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to update test")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, parseTestModel(updated))
}

// @Security Bearer
// @Router /tests/image-upload [post]
// @Summary Upload an image
// @Description Upload an image
// @Tags test
// @Accept json
// @Produce json
// @Param file formData file true "File"
// @Success 200 {object} models.ResponseOK
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UploadQuestionImage(c *gin.Context) {
	var image File

	err := c.ShouldBind(&image)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	fileFormat := filepath.Ext(image.File.Filename)
	if fileFormat != ".png" && fileFormat != ".jpeg" && fileFormat != ".jpg" {
		h.logger.WithError(err).Error("incompatible file format")
		c.JSON(http.StatusBadRequest, errorResponse(errors.New("incompatible file format")))
		return
	}

	dst, _ := os.Getwd()
	if _, err := os.Stat(dst + "/media"); os.IsNotExist(err) {
		os.Mkdir(dst+"/media", os.ModePerm)
	}

	id := uuid.New()
	fileName := id.String() + filepath.Ext(image.File.Filename)

	filePath := "/media/" + fileName
	err = c.SaveUploadedFile(image.File, dst+filePath)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, models.ResponseOK{
		Message: filePath,
	})
}

// @Security Bearer
// @Router /tests/by/category [get]
// @Summary Get tests by categoryID
// @Description Get tests by categoryID and optional by difficulty and language
// @Tags test
// @Accept json
// @Produce json
// @Param filter query models.GetTestsParams false "Filter"
// @Success 200 {object} models.GetTestsResp
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetTestsByCategory(c *gin.Context) {
	req, err := validateTestsParams(c)
	if err != nil {
		h.logger.WithError(err).Error("failed to get params")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	result, err := h.storage.Test().GetTestsByCategory(&repo.GetTestsParams{
		CategoryID: req.CategoryID,
		Difficulty: req.Difficulty,
		Lang:       req.Lang,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to get random tests")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	for i := 0; i < len(result.Tests); i++ {
		result.Tests[i] = h.makeFullUrls(result.Tests[i])
	}

	c.JSON(http.StatusOK, getTestsResponse(result))
}

// @Router /tests/get-one/{id} [get]
// @Summary Get single test
// @Description Get single test by its id
// @Tags test
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.Test
// @Failure 404 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetSingleTest(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		h.logger.WithError(err).Error("failed get an id")
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	resp, err := h.storage.Test().Get(int64(id))
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			h.logger.WithError(err).Error("test not found")
			c.JSON(http.StatusNotFound, errorResponse(err))
			return
		} else {
			h.logger.WithError(err).Error("failed get test")
			c.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
	}

	c.JSON(http.StatusOK, parseTestModel(h.makeFullUrls(resp)))
}

// @Router /tests/check-answers [post]
// @Summary Check tests answers
// @Description Check 30 tests answers
// @Tags test
// @Accept json
// @Produce json
// @Param tests body repo.CheckTestsAnswersReq true "Tests"
// @Success 200 {object} repo.CheckTestsAnswersResp
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CheckTestsAnswers(c *gin.Context) {
	var (
		req repo.CheckTestsAnswersReq
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	checkedTests, err := h.storage.Test().CheckAnswers(&req)
	if err != nil {
		h.logger.WithError(err).Error("failed to get checked tests response")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, checkedTests)
}
