package v1

import (
	"database/sql"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/everest_project/backend/api/models"
	"gitlab.com/everest_project/backend/pkg/token"
)

// @Router /token/refresh [post]
// @Summary Refresh Token
// @Description Register Token
// @Tags refresh_token
// @ID RefreshToken
// @Accept json
// @Produce json
// @Param data body models.RenewAccessTokenReq true "Data"
// @Success 200 {object} models.RenewAccessTokenResponse
// @Failure 500 {object} models.ErrorResponse
// @Failure 400 {object} models.ErrorResponse
func (h *handlerV1) RenewAccessToken(ctx *gin.Context) {
	var req models.RenewAccessTokenReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.logger.WithError(err).Error("failed to bind json")
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	refreshPayload, err := h.tokenMaker.VerifyToken(req.RefreshToken)
	if err != nil {
		h.logger.WithError(err).Error("failed to verify token")
		ctx.JSON(http.StatusUnauthorized, errorResponse(err))
		return
	}

	session, err := h.storage.Session().Get(refreshPayload.ID)
	if err != nil {
		if err == sql.ErrNoRows {
			h.logger.WithError(err).Error("failed to get session")
			ctx.JSON(http.StatusNotFound, errorResponse(err))
			return
		}
		h.logger.WithError(err).Error("failed to get session")
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	if session.IsBlocked {
		err := fmt.Errorf("blocked session")
		h.logger.WithError(err).Error("session is blocked")
		ctx.JSON(http.StatusUnauthorized, errorResponse(err))
		return
	}

	if session.Email != refreshPayload.Email {
		err := fmt.Errorf("incorrect session email")
		h.logger.WithError(err).Error("incorrect email")
		ctx.JSON(http.StatusUnauthorized, errorResponse(err))
		return
	}

	if session.RefreshToken != req.RefreshToken {
		err := fmt.Errorf("mismatched session token")
		h.logger.WithError(err).Error("mismatched session token")
		ctx.JSON(http.StatusUnauthorized, errorResponse(err))
		return
	}

	if time.Now().After(session.ExpiresAt) {
		err := fmt.Errorf("expired session")
		h.logger.WithError(err).Error("expired session")
		ctx.JSON(http.StatusUnauthorized, errorResponse(err))
		return
	}

	accessToken, accessPayload, err := h.tokenMaker.CreateToken(&token.TokenParams{
		UserID:   refreshPayload.UserID,
		Email:    refreshPayload.Email,
		UserType: refreshPayload.UserType,
		Duration: h.cfg.AccessTokenDuration,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to create token")
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	rsp := models.RenewAccessTokenResponse{
		AccessToken:          accessToken,
		AccessTokenExpiresAt: accessPayload.ExpiresAt,
	}

	ctx.JSON(http.StatusOK, rsp)
}
