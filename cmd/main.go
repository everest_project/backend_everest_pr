package main

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/redis/go-redis/v9"
	"gitlab.com/everest_project/backend/api"
	"gitlab.com/everest_project/backend/config"
	logging "gitlab.com/everest_project/backend/pkg/logger"
	"gitlab.com/everest_project/backend/pkg/token"
	"gitlab.com/everest_project/backend/storage"
)

func main() {
	cfg := config.Load(".")

	logging.Init()
	log := logging.GetLogger()

	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	psqlConn, err := sqlx.Connect("postgres", psqlUrl)
	if err != nil {
		log.Fatalf("failed to connect to database: %v", err)
	}

	rdb := redis.NewClient(&redis.Options{
		Addr: cfg.Redis.Addr,
	})

	strg := storage.NewStoragePg(psqlConn)
	inMemory := storage.NewInMemoryStorage(rdb)
	maker, err := token.NewJWTMaker(cfg.TokenSymmetricKey)
	if err != nil {
		log.Fatalf("failed to get new JWT maker %v", err)
	}

	apiServer := api.New(&api.RouterOptions{
		Cfg:        &cfg,
		Storage:    strg,
		InMemory:   inMemory,
		TokenMaker: maker,
		Logger:     &log,
	})

	err = apiServer.Run(cfg.HttpPort)
	if err != nil {
		log.Fatalf("failed to run server: %v", err)
	}
	log.Print("Server stopped")
	
}
