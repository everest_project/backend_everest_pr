CREATE TABLE IF NOT EXISTS "categories" (
    "id" SERIAL PRIMARY KEY,
    "title" VARCHAR(100) NOT NULL,
    "created_at" TIMESTAMPTZ DEFAULT (NOW()),
    UNIQUE("title")
);

CREATE TABLE IF NOT EXISTS "tests" (
    "id" SERIAL PRIMARY KEY,
    "admin_id" INT NOT NULL,
    "category_id" INT NOT NULL REFERENCES categories(id) ON DELETE CASCADE,
    "difficulty" VARCHAR(40) CHECK ("difficulty" IN ('medium', 'hard')) NOT NULL, 
    "question" VARCHAR(300) NOT NULL,
    "question_image_url" VARCHAR,
    "options" JSONB,
    "answer_option" VARCHAR(300) NOT NULL,
    "points" INT,
    "lang" VARCHAR(10) CHECK ("lang" IN ('uz', 'ru')) NOT NULL,
    "created_at" TIMESTAMPTZ DEFAULT (NOW())
);


CREATE TABLE IF NOT EXISTS "users" (
    "id" SERIAL PRIMARY KEY,
    "first_name" VARCHAR(30) NOT NULL,
    "last_name" VARCHAR(40) NOT NULL,
    "username" VARCHAR(20),
    "email" VARCHAR(40) NOT NULL,
    "phone_number" VARCHAR(20),
    "password" VARCHAR NOT NULL,
    "points" INT DEFAULT 0,
    "type" VARCHAR(40) CHECK ("type" IN ('admin', 'user')) NOT NULL,
    "created_at" TIMESTAMPTZ DEFAULT (NOW()),
    UNIQUE("email", "username", "phone_number")
);

CREATE TABLE "sessions" (
    "id" uuid PRIMARY KEY,
    "email" VARCHAR NOT NULL,
    "refresh_token" VARCHAR NOT NULL,
    "user_agent" VARCHAR NOT NULL,
    "client_ip" VARCHAR NOT NULL,
    "is_blocked" boolean NOT NULL DEFAULT false,
    "expires_at" timestamp not null,
    "created_at" TIMESTAMPTZ DEFAULT (NOW())
);

INSERT INTO users(first_name, last_name, email, password, type)
VALUES('Mirasil', 'Mirziyodov', 'asilfr.dev@gmail.com', '$2a$10$FNLsBJJ4MlDSmRhw.8UPoOGk.7R6d5VdBATv8Hq4fJ1.D9ae38zie', 'admin');

INSERT INTO users(first_name, last_name, email, password, type)
VALUES('Abror', 'Rahmatov', 'rabrorbek8@gmail.com', '$2a$10$FNLsBJJ4MlDSmRhw.8UPoOGk.7R6d5VdBATv8Hq4fJ1.D9ae38zie', 'admin');