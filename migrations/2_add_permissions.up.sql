CREATE TABLE IF NOT EXISTS permissions(
    id SERIAL PRIMARY KEY,
    user_type VARCHAR CHECK ("user_type" IN('admin', 'user')) NOT NULL,
    resource VARCHAR NOT NULL,
    action VARCHAR NOT NULL,
  UNIQUE(user_type, resource, action)  
);

INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'users', 'create');
INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'users', 'update');
INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'users', 'get');
INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'users', 'get-all');
INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'users', 'delete');
INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'users', 'get-profile');
INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'auth', 'update-password');
INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'auth', 'update-email');
INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'auth', 'logout');
INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'auth', 'delete-account');

INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'tests', 'add');
INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'tests', 'get-by-category');
INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'tests', 'delete');
INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'test', 'delete');
INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'tests', 'update');
INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'tests', 'image-upload');

INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'categories', 'create');
INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'categories', 'update');
INSERT INTO permissions(user_type, resource, action) VALUES ('admin', 'categories', 'delete');

INSERT INTO permissions(user_type, resource, action) VALUES ('user', 'users', 'get-all');
INSERT INTO permissions(user_type, resource, action) VALUES ('user', 'users', 'get');
INSERT INTO permissions(user_type, resource, action) VALUES ('user', 'auth', 'update-password');
INSERT INTO permissions(user_type, resource, action) VALUES ('user', 'auth', 'update-email');
INSERT INTO permissions(user_type, resource, action) VALUES ('user', 'auth', 'logout');
INSERT INTO permissions(user_type, resource, action) VALUES ('user', 'auth', 'delete-account');
INSERT INTO permissions(user_type, resource, action) VALUES ('user', 'users', 'get-profile');
INSERT INTO permissions(user_type, resource, action) VALUES ('user', 'users', 'delete');
