package postgres

import (
	"database/sql"

	"github.com/jmoiron/sqlx"
	"gitlab.com/everest_project/backend/storage/repo"
)

type categoryRepo struct {
	db *sqlx.DB
}

func NewCategory(db *sqlx.DB) repo.CategoryStorageI {
	return &categoryRepo{
		db: db,
	}
}

func (cr *categoryRepo) Create(b *repo.Category) (*repo.Category, error) {
	query := `
			INSERT INTO categories (
				title
			) VALUES ($1)
			RETURNING id, created_at
		`

	err := cr.db.QueryRow(
		query,
		b.Title,
	).Scan(
		&b.ID,
		&b.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func (cr *categoryRepo) Get(id int64) (*repo.Category, error) {
	var c repo.Category
	query := `
		SELECT
			id,
			title,
			created_at
		FROM categories 
		WHERE id=$1
	`

	err := cr.db.QueryRow(
		query,
		id,
	).Scan(
		&c.ID,
		&c.Title,
		&c.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &c, nil

}

func (cr *categoryRepo) GetAll(param *repo.GetAllCategoriesParam) (*repo.GetAllCategoriesRes, error) {
	result := repo.GetAllCategoriesRes{
		Categories: make([]*repo.Category, 0),
	}

	filter := ""
	if param.Title != "" {
		str := param.Title
		filter += " WHERE title ILIKE '%" + str + "%' "
	}

	query := `
		SELECT
			id,
			title,
			created_at
		FROM categories	
		` + filter + `
		ORDER BY created_at desc
	`

	rows, err := cr.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			c repo.Category
		)

		err := rows.Scan(
			&c.ID,
			&c.Title,
			&c.CreatedAt,
		)
		if err != nil {
			return nil, err
		}

		result.Categories = append(result.Categories, &c)
	}

	queryCount := `SELECT count(1) FROM categories` + filter
	err = cr.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (cr *categoryRepo) Update(book *repo.Category) (*repo.Category, error) {
	query := `
		UPDATE categories SET
			title=$1
		WHERE id=$2
		RETURNING created_at
	`
	err := cr.db.QueryRow(
		query,
		book.Title,
		book.ID,
	).Scan(
		&book.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return book, nil
}

func (cr *categoryRepo) Delete(id int64) error {
	query := `
		DELETE FROM categories WHERE id=$1
	`
	
	res, err := cr.db.Exec(query, id)
	if err != nil {
		return err
	}

	if result, _ := res.RowsAffected(); result == 0 {
		return sql.ErrNoRows
	}

	return nil

}
