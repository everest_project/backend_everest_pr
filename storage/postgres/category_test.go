package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/everest_project/backend/storage/repo"
)

func createCategory(t *testing.T) *repo.Category {
	title := faker.Word()
	c, err := strg.Category().Create(&repo.Category{
		Title: title,
	})
	require.NoError(t, err)
	require.NotEmpty(t, c)

	return c
}

func TestCreateCategory(t *testing.T) {
	category := createCategory(t)
	require.NotEmpty(t, category)
	deleteCategory(t, category.ID)
}

func deleteCategory(t *testing.T, id int64) {
	err := strg.Category().Delete(id)
	require.NoError(t, err)
	require.Empty(t, err)
}

func TestGetAllCategory(t *testing.T) {
	ids := []int64{}
	for i := 0; i < 10; i++ {
		c := createCategory(t)
		ids = append(ids, c.ID)
	}
	resp, err := strg.Category().GetAll(&repo.GetAllCategoriesParam{})
	count := 0
	for i := 0; i < len(resp.Categories); i++ {
		count++
	}
	require.GreaterOrEqual(t, count, 10)

	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteCategory(t, ids[i])
	}
}
