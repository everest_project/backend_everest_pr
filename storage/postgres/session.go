package postgres

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/everest_project/backend/storage/repo"
)

type sessionRepo struct {
	db *sqlx.DB
}

func NewSession(db *sqlx.DB) repo.SessionStorageI {
	return &sessionRepo{
		db: db,
	}
}

func (sr *sessionRepo) Create(s *repo.Session) (*repo.Session, error) {
	_, err := getByEmail(sr.db, s.Email)
	if errors.Is(err, sql.ErrNoRows) {
		query := `
			INSERT INTO sessions (
				id,
				email,
				refresh_token,
				user_agent,
				client_ip,
				is_blocked,
				expires_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7)
			RETURNING created_at
		`

		err := sr.db.QueryRow(
			query,
			s.ID,
			s.Email,
			s.RefreshToken,
			s.UserAgent,
			s.ClientIP,
			s.IsBlocked,
			s.ExpiresAt,
		).Scan(&s.CreatedAt)
		if err != nil {
			return nil, err
		}
	} else {
		query := `
			UPDATE sessions SET
				id = $1,
				email = $2,
				refresh_token = $3,
				user_agent = $4,
				client_ip = $5,
				is_blocked = $6,
				expires_at = $7,
				created_at = CURRENT_TIMESTAMP 
			WHERE email = $8 RETURNING created_at
		`
		err = sr.db.QueryRow(
			query,
			s.ID,
			s.Email,
			s.RefreshToken,
			s.UserAgent,
			s.ClientIP,
			s.IsBlocked,
			s.ExpiresAt,
			s.Email,
		).Scan(&s.CreatedAt)
		if err != nil {
			return nil, err
		}
	}

	return s, nil

}

func getByEmail(db *sqlx.DB, email string) (*repo.Session, error) {
	var s repo.Session
	query := "SELECT id FROM sessions WHERE email = $1"

	err := db.QueryRow(
		query,
		email,
	).Scan(
		&s.ID,
	)
	if err != nil {
		return nil, err
	}

	return &s, nil
}

func (sr *sessionRepo) Get(id uuid.UUID) (*repo.Session, error) {
	var s repo.Session
	query := `
		SELECT
			id,
			email,
			refresh_token,
			user_agent,
			client_ip,
			is_blocked,
			expires_at,
			created_at
		FROM sessions 
		WHERE id = $1
	`

	err := sr.db.QueryRow(
		query,
		id,
	).Scan(
		&s.ID,
		&s.Email,
		&s.RefreshToken,
		&s.UserAgent,
		&s.ClientIP,
		&s.IsBlocked,
		&s.ExpiresAt,
		&s.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &s, nil

}

func (sr *sessionRepo) GetByEmail(params *repo.GetSessionParams) (*repo.Session, error) {
	var s repo.Session

	filter := ""
	if params.UserAgent != "" {
		filter += fmt.Sprintf(" AND user_agent='%s' ", params.UserAgent)
	}

	query := `
		SELECT
			id,
			email,
			refresh_token,
			user_agent,
			client_ip,
			is_blocked,
			expires_at,
			created_at
		FROM sessions 
		WHERE email=$1
	` + filter

	err := sr.db.QueryRow(
		query,
		params.Email,
	).Scan(
		&s.ID,
		&s.Email,
		&s.RefreshToken,
		&s.UserAgent,
		&s.ClientIP,
		&s.IsBlocked,
		&s.ExpiresAt,
		&s.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &s, nil

}

func (sr *sessionRepo) Delete(params *repo.GetSessionParams) error {
	filter := ""
	if params.UserAgent != "" {
		filter += fmt.Sprintf(" AND user_agent='%s' ", params.UserAgent)
	}

	query := `
		DELETE FROM sessions WHERE email=$1 ` + filter

	res, err := sr.db.Exec(
		query,
		params.Email,
	)
	if err != nil {
		return err
	}

	if result, _ := res.RowsAffected(); result == 0 {
		return sql.ErrNoRows
	}

	return nil

}
