package postgres_test

import (
	"testing"
	"time"

	"github.com/bxcodec/faker/v4"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/everest_project/backend/storage/repo"
)

func createSession(t *testing.T) *repo.Session {
	session := repo.Session{
		ID:           uuid.New(),
		Email:        faker.Email(),
		RefreshToken: faker.Word(),
		UserAgent:    faker.Word(),
		ClientIP:     faker.IPv6(),
		IsBlocked:    false,
		ExpiresAt:    time.Now().Add(time.Hour * 24),
	}
	res, err := strg.Session().Create(&session)
	require.NoError(t, err)
	require.NotEmpty(t, res)
	require.Equal(t, session.ID, res.ID)
	require.Equal(t, session.RefreshToken, res.RefreshToken)
	require.Equal(t, session.Email, res.Email)
	require.Equal(t, session.ClientIP, res.ClientIP)
	require.Equal(t, session.UserAgent, res.UserAgent)

	return res
}

func TestCreateSession(t *testing.T) {
	session := createSession(t)
	deleteSession(t, session.Email)
}

func deleteSession(t *testing.T, email string) {
	err := strg.Session().Delete(&repo.GetSessionParams{Email: email})
	require.NoError(t, err)
}

func TestGetSession(t *testing.T) {
	session := createSession(t)
	res, err := strg.Session().Get(session.ID)
	require.NoError(t, err)
	require.NotEmpty(t, res)
	require.Equal(t, session.ID, res.ID)
	require.Equal(t, session.Email, res.Email)
	require.Equal(t, session.RefreshToken, res.RefreshToken)
	require.Equal(t, session.Email, res.Email)
	require.Equal(t, session.UserAgent, res.UserAgent)
}

func TestGetSessionByEmail(t *testing.T) {
	session := createSession(t)
	res, err := strg.Session().GetByEmail(&repo.GetSessionParams{Email: session.Email})
	require.NoError(t, err)
	require.NotEmpty(t, res)
	require.Equal(t, session.ID, res.ID)
	require.Equal(t, session.Email, res.Email)
	require.Equal(t, session.RefreshToken, res.RefreshToken)
	require.Equal(t, session.Email, res.Email)
	require.Equal(t, session.UserAgent, res.UserAgent)

	deleteSession(t, session.Email)
}
