package postgres

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/jmoiron/sqlx"
	"gitlab.com/everest_project/backend/pkg/utils"
	"gitlab.com/everest_project/backend/storage/repo"
)

type testRepo struct {
	db *sqlx.DB
}

func NewTest(db *sqlx.DB) repo.TestStorageI {
	return &testRepo{
		db: db,
	}
}

func (tr *testRepo) MultipleInsert(tests []repo.Test) error {

	query := `
		INSERT INTO tests (
			admin_id,
			category_id,
			difficulty,
			question,
			question_image_url,
			options,
			answer_option,
			points,
			lang
		) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)
	`
	stmt, err := tr.db.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	tx, err := tr.db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		}
	}()
	ptrs := make([]interface{}, 9)
	for i := range ptrs {
		ptrs[i] = new(interface{})
	}

	for _, row := range tests {
		options, err := json.Marshal(row.Options)
		if err != nil {
			return err
		}

		*ptrs[0].(*interface{}) = row.AdminID
		*ptrs[1].(*interface{}) = row.CategoryID
		*ptrs[2].(*interface{}) = row.Difficulty
		*ptrs[3].(*interface{}) = row.Question
		*ptrs[4].(*interface{}) = row.QuestionImageUrl
		*ptrs[5].(*interface{}) = options
		*ptrs[6].(*interface{}) = row.AnswerOption
		*ptrs[7].(*interface{}) = row.Points
		*ptrs[8].(*interface{}) = row.Lang

		_, err = stmt.Exec(ptrs...)
		if err != nil {
			return err
		}
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func (tr *testRepo) Create(test *repo.Test) error {
	options, err := json.Marshal(&test.Options)
	if err != nil {
		return err
	}
	query := `
		INSERT INTO tests (
			admin_id,
			category_id,
			difficulty,
			question,
			question_image_url,
			options,
			answer_option,
			points,
			lang
		) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)
	`

	res, err := tr.db.Exec(
		query,
		test.AdminID,
		test.CategoryID,
		test.Difficulty,
		test.Question,
		test.QuestionImageUrl,
		options,
		test.AnswerOption,
		test.Points,
		test.Lang,
	)
	if err != nil {
		return err
	}

	if rows, _ := res.RowsAffected(); rows == 0 {
		return err
	}

	return nil
}

func (tr *testRepo) Get(id int64) (*repo.Test, error) {
	var (
		result repo.Test
	)

	query := `
		SELECT
			id,
			admin_id,
			category_id,
			difficulty,
			question,
			question_image_url,
			options,
			answer_option,
			points,
			lang,
			created_at
		FROM tests
		WHERE id=$1
	`

	var options interface{}
	row := tr.db.QueryRow(query, id)
	err := row.Scan(
		&result.ID,
		&result.AdminID,
		&result.CategoryID,
		&result.Difficulty,
		&result.Question,
		&result.QuestionImageUrl,
		&options,
		&result.AnswerOption,
		&result.Points,
		&result.Lang,
		&result.CreatedAt,
	)
	if err != nil {
		return nil, err
	}
	o, ok := options.([]byte)
	if !ok {
		return nil, errors.New("type assertion to []byte failed")
	}

	err = json.Unmarshal(o, &result.Options)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (tr *testRepo) GetTestsIDs(params *repo.GetTestsParams) ([]int64, error) {
	var ids []int64

	filter := fmt.Sprintf(`WHERE category_id='%d' 
		AND difficulty='%s' AND lang='%s' `,
		params.CategoryID, params.Difficulty, params.Lang,
	)

	query := `
		SELECT
			id
		FROM tests
		` + filter + `
		ORDER BY created_at desc
	`
	rows, err := tr.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	for rows.Next() {
		var (
			id int64
		)

		err := rows.Scan(&id)
		if err != nil {
			return nil, err
		}

		ids = append(ids, id)
	}

	return ids, nil
}

func (tr *testRepo) GetTestsUrls(params *repo.GetTestsParams) ([]*repo.Test, error) {
	var tests []*repo.Test
	filter := "WHERE true "
	if params.CategoryID != 0 {
		filter += fmt.Sprintf(`AND category_id='%d' `, params.CategoryID)
	}
	if params.Difficulty != "" {
		filter += fmt.Sprintf(`AND difficulty='%s' `, params.Difficulty)
	}
	if params.Lang != "" {
		filter += fmt.Sprintf(`AND lang='%s' `, params.Lang)
	}

	query := `
		SELECT
			question_image_url,
			options
		FROM tests
		` + filter + `
		ORDER BY created_at desc
		`

	rows, err := tr.db.Query(query)
	if err != nil {
		return nil, err
	}
	if rows == nil {
		return nil, sql.ErrNoRows
	}

	defer rows.Close()
	for rows.Next() {
		var (
			res     repo.Test
			options interface{}
		)
		err := rows.Scan(&res.QuestionImageUrl, &options)
		if err != nil {
			return nil, err
		}

		o, ok := options.([]byte)
		if !ok {
			return nil, errors.New("type assertion to []byte failed")
		}

		err = json.Unmarshal(o, &res.Options)
		if err != nil {
			return nil, err
		}
		tests = append(tests, &res)
	}

	return tests, nil
}

func (tr *testRepo) Get30Tests(params *repo.GetTestsParams) (*repo.GetTestsRes, error) {
	res := &repo.GetTestsRes{
		Tests: make([]*repo.Test, 0),
	}
	ids, err := tr.GetTestsIDs(&repo.GetTestsParams{
		CategoryID: params.CategoryID,
		Difficulty: params.Difficulty,
		Lang:       params.Lang,
	})
	if err != nil {
		return nil, err
	}
	randomIDs := utils.RandomizeSlice(ids)
	if err != nil {
		return nil, err
	}

	for index, id := range randomIDs {
		if index >= 30 {
			break
		}
		test, err := tr.Get(int64(id))
		if err != nil {
			return nil, err
		}
		res.Tests = append(res.Tests, test)
	}
	res.Count = int32(len(res.Tests))

	return res, nil
}

func (tr *testRepo) CheckAnswers(data *repo.CheckTestsAnswersReq) (*repo.CheckTestsAnswersResp, error) {
	res := &repo.CheckTestsAnswersResp{
		CheckedTests: make([]*repo.CheckedTest, 0),
	}
	for index, v := range data.Tests {
		if index >= 30 {
			break
		}
		var checkedTest repo.CheckedTest
		test, err := tr.Get(v.TestID)
		if err != nil {
			return nil, err
		}
		checkedTest.TestID = test.ID
		checkedTest.Answer = test.AnswerOption
		if v.ChosenOption != test.AnswerOption {
			checkedTest.IsTrue = false
		} else {
			checkedTest.IsTrue = true
		}
		res.CheckedTests = append(res.CheckedTests, &checkedTest)
	}
	res.Count = int32(len(res.CheckedTests))

	return res, nil
}

func (tr *testRepo) GetTestsByCategory(params *repo.GetTestsParams) (*repo.GetTestsRes, error) {
	res := &repo.GetTestsRes{
		Tests: make([]*repo.Test, 0),
	}
	filter := fmt.Sprintf(`WHERE category_id = '%d' `, params.CategoryID)
	if params.Difficulty != "" {
		filter += fmt.Sprintf(`AND difficulty = '%s' `, params.Difficulty)
	}
	if params.Lang != "" {
		filter += fmt.Sprintf(`AND lang = '%s' `, params.Lang)
	}

	query := `
		SELECT
			id,
			admin_id,
			category_id,
			difficulty,
			question,
			question_image_url,
			options,
			answer_option,
			points,
			lang,
			created_at
		FROM tests
		` + filter + `
		ORDER BY created_at asc
		`
	rows, err := tr.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	for rows.Next() {
		var (
			t repo.Test
		)
		var options interface{}
		err := rows.Scan(
			&t.ID,
			&t.AdminID,
			&t.CategoryID,
			&t.Difficulty,
			&t.Question,
			&t.QuestionImageUrl,
			&options,
			&t.AnswerOption,
			&t.Points,
			&t.Lang,
			&t.CreatedAt,
		)
		if err != nil {
			return nil, err
		}
		o, ok := options.([]byte)
		if !ok {
			return nil, errors.New("type assertion to []byte failed")
		}

		err = json.Unmarshal(o, &t.Options)
		if err != nil {
			return nil, err
		}

		res.Tests = append(res.Tests, &t)
	}

	queryCount := `SELECT count(1) FROM tests ` + filter
	err = tr.db.QueryRow(queryCount).Scan(&res.Count)
	if err != nil {
		return nil, err
	}

	return res, nil
}
func (tr *testRepo) Update(t *repo.Test) (*repo.Test, error) {
	query := `
		UPDATE tests SET
			category_id=$1,
			difficulty=$2,
			question=$3,
			question_image_url=$4,
			options=$5,
			answer_option=$6,
			points=$7,
			lang=$8
		WHERE id=$9
		RETURNING admin_id, created_at
	`

	options, err := json.Marshal(t.Options)
	if err != nil {
		return nil, err
	}

	err = tr.db.QueryRow(
		query,
		t.CategoryID,
		t.Difficulty,
		t.Question,
		utils.NullString(*t.QuestionImageUrl),
		options,
		t.AnswerOption,
		t.Points,
		t.Lang,
		t.ID,
	).Scan(&t.AdminID, &t.CreatedAt)
	if err != nil {
		return nil, err
	}

	return t, nil
}

func (tr *testRepo) Delete(id, categoryID int64) error {
	test, err := tr.Get(id)
	if err != nil {
		return err
	}
	err = tr.DeleteImages(test)
	if err != nil {
		return err
	}

	query := "DELETE FROM tests WHERE id=$1 AND category_id=$2"
	result, err := tr.db.Exec(query, id, categoryID)
	if err != nil {
		return err
	}

	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (tr *testRepo) DeleteByCategoryNDifficulty(categoryId int64, difficulty, lang string) error {
	tests, err := tr.GetTestsUrls(&repo.GetTestsParams{
		CategoryID: categoryId,
		Difficulty: difficulty,
		Lang:       lang,
	})
	if err != nil {
		return err
	}

	for _, data := range tests {
		err := tr.DeleteImages(data)
		if err != nil {
			return err
		}
	}

	query := `
		DELETE FROM tests 
		WHERE category_id=$1 AND difficulty=$2 AND lang=$3
	`

	res, err := tr.db.Exec(
		query,
		categoryId,
		difficulty,
		lang,
	)
	if err != nil {
		return err
	}
	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (tr *testRepo) DeleteImages(data *repo.Test) error {
	dst, _ := os.Getwd()
	for _, option := range data.Options {
		optionImage := strings.Split(*option.ImageUrl, "/")
		if len(optionImage) > 1 {
			pathToOptionImage := dst + "/media/" + optionImage[2]
			if err := os.Remove(pathToOptionImage); err != nil {
				return err
			} else {
				continue
			}
		} else {
			continue
		}
	}
	if data.QuestionImageUrl != nil {
		questionImages := strings.Split(*data.QuestionImageUrl, "/")
		pathToQuestionImage := dst + "/media/" + questionImages[2]
		if err := os.Remove(pathToQuestionImage); err != nil {
			return err
		}
	}

	return nil
}
