package postgres_test

import (
	"log"
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/everest_project/backend/pkg/utils"
	"gitlab.com/everest_project/backend/storage/repo"
)

func createTest(t *testing.T, categoryId, adminId int64, difficulty, lang string) {
	options := []*repo.Option{
		{
			ID:    1,
			Title: faker.Word(),
		},
		{
			ID:    2,
			Title: faker.Word(),
		},
		{
			ID:    3,
			Title: "C",
		},
		{
			ID:    4,
			Title: faker.Word(),
		},
	}
	err := strg.Test().Create(&repo.Test{
		AdminID:      adminId,
		CategoryID:   categoryId,
		Difficulty:   difficulty,
		Question:     faker.Sentence(),
		Options:      options,
		AnswerOption: "C",
		Points:       int32(utils.RandomInt(1, 20)),
	})
	require.NoError(t, err)
}

func TestCreateTest(t *testing.T) {
	user := createUser(t)
	category := createCategory(t)
	createTest(t, category.ID, user.ID, "hard", "uz")
}

func TestGetTest(t *testing.T) {
	test, err := strg.Test().Get(10)
	require.NoError(t, err)
	for _, v := range test.Options {
		log.Print(v)
	}
	require.NoError(t, err)
	require.NoError(t, err)
	require.NotEmpty(t, test)
}

func TestGet30RandomIds(t *testing.T) {
	category := createCategory(t)

	ids, err := strg.Test().Get30Tests(&repo.GetTestsParams{
		CategoryID: category.ID,
		Difficulty: "medium",
	})
	require.NoError(t, err)
	require.NotEmpty(t, ids)
	deleteCategory(t, category.ID)
}

func TestGet30Tests(t *testing.T) {
	// user := createUser(t)
	// c := createCategory(t)
	// for i := 0; i < 30; i++ {
	// 	createTest(t, c.ID, user.ID, "hard", "uz")
	// }
	res, err := strg.Test().Get30Tests(&repo.GetTestsParams{
		CategoryID: 2,
		Difficulty: "medium",
		Lang:       "uz",
	})
	require.NoError(t, err)
	for _, v := range res.Tests {
		log.Print(v.ID)
	}
	require.NoError(t, err)
	require.NotEmpty(t, res)
}

func TestDeleteByCategory(t *testing.T) {
	user := createUser(t)
	c := createCategory(t)
	for i := 0; i < int(utils.RandomInt(10, 100)); i++ {
		createTest(t, c.ID, user.ID, "hard", "uz")
	}
	err := strg.Test().DeleteByCategoryNDifficulty(1, "hard", "uz")
	require.NoError(t, err)
	deleteCategory(t, c.ID)
	deleteUser(t, user.ID)
}
