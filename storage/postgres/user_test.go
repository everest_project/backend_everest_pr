package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/everest_project/backend/pkg/utils"
	"gitlab.com/everest_project/backend/storage/repo"
)

func createUser(t *testing.T) *repo.User {
	num := faker.Phonenumber()
	username := faker.Word()
	pass, err := utils.HashPassword(faker.Word())
	require.NoError(t, err)
	user := repo.User{
		FirstName:   faker.FirstName(),
		LastName:    faker.LastName(),
		PhoneNumber: &num,
		Email:       faker.Email(),
		Password:    pass,
		Username:    &username,
		Type:        "user",
	}
	res, err := strg.User().Create(&user)
	require.NoError(t, err)
	require.NotEmpty(t, res)
	require.Equal(t, user.FirstName, res.FirstName)
	require.Equal(t, user.LastName, res.LastName)
	require.Equal(t, user.PhoneNumber, res.PhoneNumber)
	require.Equal(t, user.Email, res.Email)

	return res
}

func TestCreate(t *testing.T) {
	user := createUser(t)
	deleteUser(t, user.ID)
}

func deleteUser(t *testing.T, id int64) {
	err := strg.User().Delete(id)
	require.NoError(t, err)
}

func TestUpdateUser(t *testing.T) {
	user := createUser(t)
	num := faker.Phonenumber()
	username := faker.Word()
	res, err := strg.User().Update(&repo.User{
		FirstName:   faker.FirstName(),
		LastName:    faker.LastName(),
		PhoneNumber: &num,
		Username:    &username,
		ID:          user.ID,
	})
	require.NoError(t, err)
	require.NotEmpty(t, res)
	require.NotEqual(t, user.FirstName, res.FirstName)
	require.NotEqual(t, user.LastName, res.LastName)
	require.NotEqual(t, user.PhoneNumber, res.PhoneNumber)
	require.Equal(t, user.Email, res.Email)
}

func TestGetAllUsers(t *testing.T) {
	ids := []int64{}
	for i := 0; i < 10; i++ {
		u := createUser(t)
		ids = append(ids, u.ID)
	}
	resp, err := strg.User().GetAll(&repo.GetAllUsersParams{
		Limit: 10,
		Page:  1,
	})
	count := 0
	for i := 0; i < len(resp.Users); i++ {
		count++
	}
	require.GreaterOrEqual(t, count, 10)

	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteUser(t, ids[i])
	}
}

func TestGetByEmail(t *testing.T) {
	user := createUser(t)
	res, err := strg.User().GetByEmail(user.Email)
	require.NoError(t, err)
	require.NotEmpty(t, res)
	require.Equal(t, user.FirstName, res.FirstName)
	require.Equal(t, user.LastName, res.LastName)
	require.Equal(t, user.Email, res.Email)

	deleteUser(t, user.ID)
}

func TestUpdatePass(t *testing.T) {
	u := createUser(t)
	pass, err := utils.HashPassword(faker.Word())
	require.NoError(t, err)
	err = strg.User().UpdatePassword(&repo.UpdatePassword{
		UserID:   u.ID,
		Password: pass,
	})
	require.NoError(t, err)

	deleteUser(t, u.ID)
}
