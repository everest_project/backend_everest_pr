package repo

import "time"

type CategoryStorageI interface {
	Create(c *Category) (*Category, error)
	Get(id int64) (*Category, error)
	GetAll(param *GetAllCategoriesParam) (*GetAllCategoriesRes, error)
	Delete(id int64) error
	Update(c *Category) (*Category, error)
}

type Category struct {
	ID        int64
	Title     string
	CreatedAt time.Time
}

type GetAllCategoriesParam struct {
	Title string
}

type GetAllCategoriesRes struct {
	Categories []*Category
	Count      int32
}
