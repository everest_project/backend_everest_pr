package repo

import (
	"time"

	"github.com/google/uuid"
)

type SessionStorageI interface {
	Create(s *Session) (*Session, error)
	Get(id uuid.UUID) (*Session, error)
	GetByEmail(params *GetSessionParams) (*Session, error)
	Delete(params *GetSessionParams) error
}

type Session struct {
	ID           uuid.UUID
	Email        string
	RefreshToken string
	UserAgent    string
	ClientIP     string
	IsBlocked    bool
	ExpiresAt    time.Time
	CreatedAt    time.Time
}

type GetSessionParams struct {
	Email     string
	UserAgent string
}
