package repo

import "time"

type TestStorageI interface {
	Create(t *Test) error
	MultipleInsert(rows []Test) error
	CheckAnswers(params *CheckTestsAnswersReq) (*CheckTestsAnswersResp, error)
	Get(id int64) (*Test, error)
	GetTestsIDs(params *GetTestsParams) ([]int64, error)
	GetTestsUrls(params *GetTestsParams) ([]*Test, error)
	Get30Tests(params *GetTestsParams) (*GetTestsRes, error)
	GetTestsByCategory(params *GetTestsParams) (*GetTestsRes, error)
	Update(test *Test) (*Test, error)
	Delete(id, categoryID int64) error
	DeleteByCategoryNDifficulty(categoryId int64, difficulty, lang string) error
	DeleteImages(test *Test) error
}

type Test struct {
	ID               int64
	AdminID          int64
	CategoryID       int64
	Difficulty       string
	Question         string
	QuestionImageUrl *string
	Options          []*Option
	AnswerOption     string
	Points           int32
	Lang             string
	CreatedAt        time.Time
}

type Option struct {
	ID       int64   `json:"id"`
	Title    string  `json:"title"`
	ImageUrl *string `json:"image_url"`
}

type GetTestsParams struct {
	CategoryID int64
	Difficulty string
	Lang       string
}

type GetTestsRes struct {
	Tests []*Test
	Count int32
}

type CheckTestsAnswers struct {
	TestID       int64  `json:"test_id"`
	ChosenOption string `json:"chosen_option"`
}

type CheckTestsAnswersReq struct {
	Tests []*CheckTestsAnswers `json:"tests"`
}

type CheckedTest struct {
	TestID int64  `json:"test_id"`
	IsTrue bool   `json:"is_true"`
	Answer string `json:"answer"`
}
type CheckTestsAnswersResp struct {
	CheckedTests []*CheckedTest `json:"checked_tests"`
	Count        int32          `json:"count"`
}
