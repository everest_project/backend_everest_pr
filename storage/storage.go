package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/everest_project/backend/storage/postgres"
	"gitlab.com/everest_project/backend/storage/repo"
)

type StorageI interface {
	User() repo.UserStorageI
	Permission() repo.PermissionStorageI
	Session() repo.SessionStorageI
	Category() repo.CategoryStorageI
	Test() repo.TestStorageI
}

type storagePg struct {
	userRepo       repo.UserStorageI
	permissionRepo repo.PermissionStorageI
	sessionRepo    repo.SessionStorageI
	categoryRepo   repo.CategoryStorageI
	testRepo       repo.TestStorageI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		userRepo:       postgres.NewUser(db),
		permissionRepo: postgres.NewPermission(db),
		sessionRepo:    postgres.NewSession(db),
		categoryRepo:   postgres.NewCategory(db),
		testRepo:       postgres.NewTest(db),
	}
}

func (s *storagePg) User() repo.UserStorageI {
	return s.userRepo
}

func (s *storagePg) Permission() repo.PermissionStorageI {
	return s.permissionRepo
}

func (s *storagePg) Session() repo.SessionStorageI {
	return s.sessionRepo
}

func (s *storagePg) Category() repo.CategoryStorageI {
	return s.categoryRepo
}

func (s *storagePg) Test() repo.TestStorageI {
	return s.testRepo
}
